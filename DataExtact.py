import AnalysisSingleField


def paper_per_year():
    year_paper_num_dict = dict()
    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\Papers.txt', 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            year = int(strline[3])
            if year in year_paper_num_dict.keys():
                year_paper_num_dict[year] += 1
            else:
                year_paper_num_dict[year] = 1

    with open('./result/PaperPerYear.txt', 'w') as f:
        for k, v in year_paper_num_dict.items():
            f.write(str(k) + '\t' + str(v) + '\n')


def extract_L1_info(L1_field_id):
    field_set = set()
    paper_set = set()
    candidate_paper_set = set()
    child_field_set = AnalysisSingleField.find_child_field(L1_field_id)
    for field in child_field_set:
        father_field_set = AnalysisSingleField.find_father_field(field)
        field_set = field_set.union(father_field_set)

    field_set = field_set.union(child_field_set)
    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperKeywords.txt', 'r',
              encoding='utf-8') as f2:
        for line in f2:
            strline = line.split('\t')
            f_info = strline[2].split('\n')[0]
            if f_info in field_set:
                candidate_paper_set.add(strline[0])

    with open('./result/034EC4EB_data/papers.txt', 'w', encoding='utf-8') as f1:
        with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\Papers.txt', 'r', encoding='utf-8') as f2:
            for line in f2:
                strline = line.split('\t')
                year = int(strline[3])
                if (strline[0] in candidate_paper_set) and (year < 2015):
                    f1.write(line)
                    paper_set.add(strline[0])

    with open('./result/034EC4EB_data/paper_keywords.txt', 'w', encoding='utf-8') as f1:
        with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperKeywords.txt', 'r', encoding='utf-8') as f2:
            for line in f2:
                strline = line.split('\t')
                f_info = strline[2].split('\n')[0]
                if (f_info in field_set) and (strline[0] in paper_set):
                    f1.write(line)

    # field_set = field_set.union(child_field_set)
    # with open('./result/0229BD39_data/paper_keywords.txt', 'w', encoding='utf-8') as f1:
    #     with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperKeywords.txt', 'r', encoding='utf-8') as f2:
    #         for line in f2:
    #             strline = line.split('\t')
    #             f_info = strline[2].split('\n')[0]
    #             if f_info in field_set:
    #                 f1.write(line)
    #                 paper_set.add(strline[0])
    #
    # with open('./result/0229BD39_data/papers.txt', 'w', encoding='utf-8') as f1:
    #     with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\Papers.txt', 'r', encoding='utf-8') as f2:
    #         for line in f2:
    #             strline = line.split('\t')
    #             if strline[0] in paper_set:
    #                 f1.write(line)

    with open('./result/034EC4EB_data/paper_author_affiliations.txt', 'w', encoding='utf-8') as f1:
        with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperAuthorAffiliations.txt', 'r', encoding='utf-8') as f2:
            for line in f2:
                strline = line.split('\t')
                if strline[0] in paper_set:
                    f1.write(line)


def author_per_year(year_interval, paper_interval):
    year_papers_dict = dict()
    year_author_dict = dict()
    year_author_num_dict = dict()
    paperID_author_dict = dict()
    for j in range(paper_interval[0], paper_interval[1] + 1):
        # print(j)
        year_papers_dict.clear()
        if j < 10:
            location_paper = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\papers\Papers.txt.00' + str(j)
        else:
            location_paper = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\papers\Papers.txt.0' + str(j)
        with open(location_paper, 'r', encoding='utf-8') as f:  # , encoding='utf-8'
            for line in f:
                strline = line.split('\t')
                if len(strline) > 3 and len(strline[3]) == 4:
                    year = int(strline[3])
                    if year in range(year_interval[0], year_interval[1] + 1):
                        if year in year_papers_dict.keys():
                            year_papers_dict[year].append(strline[0])
                        else:
                            year_papers_dict[year] = []
                            year_papers_dict[year].append(strline[0])

        for i in range(1, 36):
            print(j, i)
            paperID_author_dict.clear()
            if i < 10:
                location = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperAuthorAffiliations_1\PaperAuthorAffiliations.txt.00' + str(
                    i)
            else:
                location = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperAuthorAffiliations_1\PaperAuthorAffiliations.txt.0' + str(
                    i)
            with open(location, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    if len(strline) > 5:
                        if len(strline) > 3 and len(strline[1]) == 8:
                            if strline[0] in paperID_author_dict.keys():
                                paperID_author_dict[strline[0]].append(strline[1])
                            else:
                                paperID_author_dict[strline[0]] = []
                                paperID_author_dict[strline[0]].append(strline[1])

            for k, v in year_papers_dict.items():
                # print('year:', k)
                Dedupl_count = 0
                for p in v:
                    if p in paperID_author_dict.keys():
                        # if k in range(year_interval[0], year_interval[1]+1):
                        if k in year_author_dict.keys():
                            year_author_dict[k].extend(paperID_author_dict[p])
                            Dedupl_count += 1
                            if Dedupl_count == 1e5:
                                year_author_dict[k] = list(set(year_author_dict[k]))
                                Dedupl_count = 0
                        else:
                            year_author_dict[k] = []
                            year_author_dict[k].extend(paperID_author_dict[p])

                        # if k in year_author_dict.keys():
                        #     year_author_dict[k].extend(paperID_author_dict[p])
                        #     Dedupl_count += 1
                        #     if Dedupl_count == 1e5:
                        #         year_author_dict[k] = list(set(year_author_dict[k]))
                        #         Dedupl_count = 0
                        # else:
                        #     year_author_dict[k] = []
                        #     year_author_dict[k].extend(paperID_author_dict[p])

            # for k, v in year_author_dict.items():
            #     peoples = list(set(v))
            #     year_author_dict[k] = peoples.copy()

    for k, v in year_author_dict.items():
        year_author_num_dict[k] = len(set(v))

    result_loc = './result/AuthorPerYear' + str(year_interval) + '.txt'
    with open(result_loc, 'w') as f:
        for k, v in year_author_num_dict.items():
            f.write(str(k) + '\t' + str(v) + '\n')


def author_per_year_repeat():
    # 获取所有L2领域的id
    L2_field_set = set()
    L2_field_paper_dict = dict()
    paperID_year_dict = dict()
    year_PaperField_dict = dict()
    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\FieldOfStudyHierarchy.txt',
              'r',
              encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            if strline[1] == 'L2':
                L2_field_set.add(strline[0])
            elif strline[3] == 'L2':
                L2_field_set.add(strline[2])

    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperKeywords.txt',
              'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            field = strline[2].split('\n')[0]
            if field in L2_field_set:
                if field in L2_field_paper_dict.keys():
                    L2_field_paper_dict[field].append(strline[0])
                else:
                    L2_field_paper_dict[field] = []
                    L2_field_paper_dict[field].append(strline[0])

    for j in range(1, 15):
        # print(j)
        paperID_year_dict.clear()
        if j < 10:
            location_paper = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\papers_2\Papers.txt.00' + str(j)
        else:
            location_paper = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\papers_2\Papers.txt.0' + str(j)
        with open(location_paper, 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if len(strline) > 3 and len(strline[3]) == 4:
                    year = int(strline[3])
                    paperID_year_dict[strline[0]] = year

        for k, v in L2_field_paper_dict.items():
            for paper in v:
                year_PaperField_dict


def per_year_L2_field_num():
    year_L2_field_num_dict = dict()
    year_L2_field_dict = dict()
    with open('E:\PycharmProject\MAG\\result\InnovationDiffusion_2016.txt', 'r', encoding='utf-8') as f:
        for line in f:
            years = []
            strline = line.split('\t')
            strline_1 = strline[1].split('{')
            strline_2 = strline_1[1].split('}')
            strline_3 = strline_2[0].split(',')
            for i in range(len(strline_3)):
                if not strline_3[i].split(':')[0] == '':
                    year = int(strline_3[i].split(':')[0])
                    years.append(year)
            for y in years:
                if y in year_L2_field_dict.keys():
                    year_L2_field_dict[y].append(strline[0])
                else:
                    year_L2_field_dict[y] = []
                    year_L2_field_dict[y].append(strline[0])

    for k, v in year_L2_field_dict.items():
        year_L2_field_num_dict[k] = len(set(v))

    with open('./result/L2FieldPerYear.txt', 'w') as f:
        for k, v in year_L2_field_num_dict.items():
            f.write(str(k) + '\t' + str(v) + '\n')


if __name__ == "__main__":
    # paper_per_year()
    # extract_L1_info('0229BD39')
    years_range = [1990, 1998]
    papers_range = [1, 28]
    author_per_year(years_range, papers_range)
    # author_per_year_repeat()
    # per_year_L2_field_num()
    # extract_L1_info('034EC4EB')
    # author_per_year_repeat()