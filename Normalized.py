import matplotlib.pyplot as plt
from collections import Counter
import math


def IF_num(papers_location, paper_author_affiliations_location, strating_y, interval_t):
    year_paper_dict = dict()
    year_author_dict = dict()
    year_paper_per_people_dict = dict()
    paperID_author_dict = dict()
    year_average_dict = dict()
    year_average_sum_auther = dict()
    with open(papers_location, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            year = int(strline[3])
            if year in year_paper_dict.keys():
                year_paper_dict[year].append(strline[0])
            else:
                year_paper_dict[year] = []
                year_paper_dict[year].append(strline[0])

    with open(paper_author_affiliations_location, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            if len(strline) > 5:
                if len(strline) > 3 and len(strline[1]) == 8:
                    if strline[0] in paperID_author_dict.keys():
                        paperID_author_dict[strline[0]].append(strline[1])
                    else:
                        paperID_author_dict[strline[0]] = []
                        paperID_author_dict[strline[0]].append(strline[1])

    for k, v in year_paper_dict.items():
        for p in v:
            if p in paperID_author_dict.keys():
                if k in year_author_dict.keys():
                    year_author_dict[k].extend(paperID_author_dict[p].copy())
                else:
                    year_author_dict[k] = []
                    year_author_dict[k].extend(paperID_author_dict[p].copy())

    while 1:
        author_paper_num_interval_dict = dict()
        year_interval = str(strating_y) + '-' + str(strating_y + interval_t)
        print(year_interval)
        if not strating_y > 2012:
            for y in range(strating_y, strating_y+interval_t):
                if y in year_author_dict.keys():
                    temp_dict = dict(Counter(year_author_dict[y]))
                    for k, v in temp_dict.items():
                        if k in author_paper_num_interval_dict.keys():
                            author_paper_num_interval_dict[k] += v
                        else:
                            author_paper_num_interval_dict[k] = v

            min_IF_num = list(author_paper_num_interval_dict.values()).count(1)
            max_IF = max(list(author_paper_num_interval_dict.values()))
            Denominator = math.pow(min_IF_num, 1/2.5)
            print(Denominator)
            for k in author_paper_num_interval_dict.keys():
                # author_paper_num_interval_dict[k] = (author_paper_num_interval_dict[k]) / Denominator
                # author_paper_num_interval_dict[k] = (author_paper_num_interval_dict[k]) / max_IF
                pass
            number_num_dict = dict(Counter(author_paper_num_interval_dict.values()))
            plt.scatter(number_num_dict.keys(), number_num_dict.values(), s=10, label=year_interval)
            strating_y += interval_t
        else:
            break

    plt.xlabel('IF')
    plt.ylabel('n')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim([0.1, 10000])
    plt.legend(loc='upper right')
    plt.show()


def IF_num_MAG_rank(papers_location, paper_author_affiliations_location, strating_y, interval_t):
    year_paper_dict = dict()
    year_author_dict = dict()
    year_paper_per_people_dict = dict()
    paperID_author_dict = dict()
    paperID_rank_dict = dict()
    year_average_dict = dict()
    year_average_sum_auther = dict()
    with open(papers_location, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            year = int(strline[3])
            rank = int(strline[10])
            paperID_rank_dict[strline[0]] = math.exp(rank/(-1000))
            if year in year_paper_dict.keys():
                year_paper_dict[year].append(strline[0])
            else:
                year_paper_dict[year] = []
                year_paper_dict[year].append(strline[0])

    with open(paper_author_affiliations_location, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            if len(strline) > 5:
                if len(strline) > 3 and len(strline[1]) == 8:
                    if strline[0] in paperID_author_dict.keys():
                        paperID_author_dict[strline[0]].append(strline[1])
                    else:
                        paperID_author_dict[strline[0]] = []
                        paperID_author_dict[strline[0]].append(strline[1])

    # for k, v in year_paper_dict.items():
    #     for p in v:
    #         if p in paperID_author_dict.keys():
    #             if k in year_author_dict.keys():
    #                 year_author_dict[k].extend(paperID_author_dict[p].copy())
    #             else:
    #                 year_author_dict[k] = []
    #                 year_author_dict[k].extend(paperID_author_dict[p].copy())

    loc = 1
    while 1:
        author_paper_num_interval_dict = dict()
        year_interval = str(strating_y) + '-' + str(strating_y + interval_t)
        print(year_interval)
        if not strating_y > 2012:
            author_IF_dict = dict()
            for y in range(strating_y, strating_y+interval_t):
                # if y in year_author_dict.keys():
                #     temp_dict = dict(Counter(year_author_dict[y]))
                #     for k, v in temp_dict.items():
                #         if k in author_paper_num_interval_dict.keys():
                #             author_paper_num_interval_dict[k] += v
                #         else:
                #             author_paper_num_interval_dict[k] = v
                if y in year_paper_dict.keys():
                    for p in year_paper_dict[y]:
                        authors = paperID_author_dict[p]
                        for a in authors:
                            if a in author_IF_dict.keys():
                                author_IF_dict[a] += paperID_rank_dict[p]
                            else:
                                author_IF_dict[a] = paperID_rank_dict[p]

            # min_IF_num = list(author_paper_num_interval_dict.values()).count(1)
            # max_IF = max(list(author_paper_num_interval_dict.values()))
            # Denominator = math.pow(min_IF_num, 1/2.5)
            # print(Denominator)
            for k in author_paper_num_interval_dict.keys():
                # author_paper_num_interval_dict[k] = (author_paper_num_interval_dict[k]) / Denominator
                # author_paper_num_interval_dict[k] = (author_paper_num_interval_dict[k]) / max_IF
                pass
            number_num_dict = dict(Counter(author_IF_dict.values()))
            plt.subplot(3, 5, loc)
            # plt.scatter(number_num_dict.keys(), number_num_dict.values(), s=10, label=year_interval)
            plt.hist(author_IF_dict.values(), 1000, label=year_interval)
            plt.xlabel('IF')
            plt.ylabel('n')
            plt.xscale('log')
            plt.yscale('log')
            # plt.xlim([1e-9, 1e-4])
            plt.legend(loc='upper right')
            strating_y += interval_t
            loc += 1
        else:
            break

    # plt.xlabel('IF')
    # plt.ylabel('n')
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.xlim([5e-6, 1e-4])
    # plt.legend(loc='upper right')
    plt.show()


if __name__ == "__main__":
    L1_ID = '0229BD39'
    papers_loc = 'E:\PycharmProject\MAG\\result\\' + L1_ID + '_data\papers.txt'
    paper_author_affiliations_loc = 'E:\PycharmProject\MAG\\result\\' + L1_ID + '_data\paper_author_affiliations.txt'
    starting_year = 1948
    interval_time = 5

    # IF_num(papers_loc, paper_author_affiliations_loc, starting_year, interval_time)
    IF_num_MAG_rank(papers_loc, paper_author_affiliations_loc, starting_year, interval_time)