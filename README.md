## About the Microsoft Academic Graph
Everyone can download it free from [here](https://kddcup2016.azurewebsites.net/Data). But this data source is not up to date, you can get the newest data from [Azure](https://www.microsoft.com/en-us/research/project/microsoft-academic-graph/).
## Experiment Environment
Language: Python3.6.6
<br>IDE: Pycharm
<br>Hardware: Intel i7, 12GB RAM

As the RAM of my PC is limited, and the data file is so big that I cannot load it directly into PC RAM, so I firstly divided these files which size bigger than 10GB into several small 
pieces, for example, the Papers.txt was divided into 14 small files which size.