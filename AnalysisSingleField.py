def find_father_field(L2_field):
    father_field_set = set()
    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\FieldOfStudyHierarchy.txt', 'r',
              encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            if strline[0] == L2_field and strline[3] == 'L0':
                father_field_set.add(strline[2])
    return father_field_set


def time_IF_diffusion():
    L2_field_ID = '08DDF80C'
    father_field_set = find_father_field(L2_field_ID)
    print(father_field_set)

    L2_field_paper = set()
    L1_field_paper = set()
    L2_field_year_paper_dict = dict()
    L1_field_year_paper_dict = dict()
    L2_field_year_author_dict = dict()
    L1_field_year_author_dict = dict()
    paperID_author_dict = dict()
    cumulation_author = set()
    current_author = set()
    L2_year_new_author_dict = dict()
    L2_year_in_IF_dict = dict()

    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperKeywords.txt',
              'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            field = strline[2].split('\n')[0]
            if field == L2_field_ID:
                L2_field_paper.add(strline[0])
            elif field in father_field_set:
                L1_field_paper.add(strline[0])

    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\Papers.txt',
              'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            if strline[0] in L2_field_paper:
                year = int(strline[3])
                if year in L2_field_year_paper_dict.keys():
                    L2_field_year_paper_dict[year].append(strline[0])
                else:
                    L2_field_year_paper_dict[year] = []
                    L2_field_year_paper_dict[year].append(strline[0])
            elif strline[0] in L1_field_paper:
                year = int(strline[3])
                if year in L1_field_year_paper_dict.keys():
                    L1_field_year_paper_dict[year].append(strline[0])
                else:
                    L1_field_year_paper_dict[year] = []
                    L1_field_year_paper_dict[year].append(strline[0])

    for i in range(1, 19):
        print(i)
        paperID_author_dict.clear()
        if i < 10:
            location = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperAuthorAffiliations\PaperAuthorAffiliations.txt.00' + str(i)
        else:
            location = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperAuthorAffiliations\PaperAuthorAffiliations.txt.0' + str(i)
        with open(location, 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if len(strline) > 5:
                    if len(strline) > 3 and len(strline[1]) == 8:
                        if strline[0] in paperID_author_dict.keys():
                            paperID_author_dict[strline[0]].append(strline[1])
                        else:
                            paperID_author_dict[strline[0]] = []
                            paperID_author_dict[strline[0]].append(strline[1])

        for k, v in L2_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L2_field_year_author_dict.keys():
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L2_field_year_author_dict[k] = []
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        for k, v in L1_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L1_field_year_author_dict.keys():
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L1_field_year_author_dict[k] = []
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

    years = list(sorted(L2_field_year_author_dict.keys()))
    print(years)
    for index in range(len(years)):
        current_author = set(L2_field_year_author_dict[years[index]])
        L2_new_author = current_author.difference(cumulation_author)
        for au in current_author:
            cumulation_author.add(au)
        L2_year_new_author_dict[years[index]] = L2_new_author

    begin_year = min(years)
    for k, v in L2_year_new_author_dict.items():
        total_num = 0
        new_num = 0
        time_interval = k - begin_year
        if time_interval < 5:
            for v_ in v:
                for y in range(time_interval + 1):
                    if k-y in L1_field_year_author_dict.keys():
                        new_num += L1_field_year_author_dict[k-y].count(v_)
            for y in range(time_interval + 1):
                if k - y in L1_field_year_author_dict.keys():
                    total_num += len(L1_field_year_author_dict[k-y])
            if not total_num == 0:
                L2_year_in_IF_dict[k] = new_num / total_num
            else:
                L2_year_in_IF_dict[k] = 0
        else:
            for v_ in v:
                for y in range(5):
                    if k-y in L1_field_year_author_dict.keys():
                        new_num += L1_field_year_author_dict[k-y].count(v_)
            for y in range(5):
                if k - y in L1_field_year_author_dict.keys():
                    total_num += len(L1_field_year_author_dict[k-y])
            if not total_num == 0:
                L2_year_in_IF_dict[k] = new_num / total_num
            else:
                L2_year_in_IF_dict[k] = 0

    with open('./result/InnovationDiffusion_in_IF_2016_Approximation_algorithm.txt', 'w') as f:
        f.write(str(L2_year_in_IF_dict))
        f.write('\n')


def find_child_field(L1_field):
    child_field_set = set()
    with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\FieldOfStudyHierarchy.txt',
              'r',
              encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            if strline[2] == L1_field and strline[1] == 'L2':
                # print(strline[2], L1_field)
                child_field_set.add(strline[0])
    return child_field_set


def time_IF_diffusion_L1twoL2(L1_field_ID):
    child_field_set = find_child_field(L1_field_ID)
    print(child_field_set)

    for c_field in child_field_set:
        L2_field_paper = set()
        L1_field_paper = set()
        L2_field_year_paper_dict = dict()
        L1_field_year_paper_dict = dict()
        L2_field_year_author_dict = dict()
        L1_field_year_author_dict = dict()
        paperID_author_dict = dict()
        cumulation_author = set()
        current_author = set()
        L2_year_new_author_dict = dict()
        L2_year_in_IF_dict = dict()
        L2_year_in_IF_max_dict = dict()
        father_field_set = find_father_field(c_field)

        with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperKeywords.txt', 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                field = strline[2].split('\n')[0]
                if field == c_field:
                    L2_field_paper.add(strline[0])
                elif field in father_field_set:
                    L1_field_paper.add(strline[0])

        with open('J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\Papers.txt', 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if strline[0] in L2_field_paper:
                    year = int(strline[3])
                    if year in L2_field_year_paper_dict.keys():
                        L2_field_year_paper_dict[year].append(strline[0])
                    else:
                        L2_field_year_paper_dict[year] = []
                        L2_field_year_paper_dict[year].append(strline[0])
                elif strline[0] in L1_field_paper:
                    year = int(strline[3])
                    if year in L1_field_year_paper_dict.keys():
                        L1_field_year_paper_dict[year].append(strline[0])
                    else:
                        L1_field_year_paper_dict[year] = []
                        L1_field_year_paper_dict[year].append(strline[0])

        for i in range(1, 19):
            print(i)
            paperID_author_dict.clear()
            if i < 10:
                location = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperAuthorAffiliations\PaperAuthorAffiliations.txt.00' + str(i)
            else:
                location = 'J:\后锦-毕业交接\\3 实验数据部分\microsoft adademic graph论文数据集\MicrosoftAcademicGraph\PaperAuthorAffiliations\PaperAuthorAffiliations.txt.0' + str(i)
            with open(location, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    if len(strline) > 5:
                        if len(strline) > 3 and len(strline[1]) == 8:
                            if strline[0] in paperID_author_dict.keys():
                                paperID_author_dict[strline[0]].append(strline[1])
                            else:
                                paperID_author_dict[strline[0]] = []
                                paperID_author_dict[strline[0]].append(strline[1])

            for k, v in L2_field_year_paper_dict.items():
                for p in v:
                    if p in paperID_author_dict.keys():
                        if k in L2_field_year_author_dict.keys():
                            L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                        else:
                            L2_field_year_author_dict[k] = []
                            L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

            for k, v in L1_field_year_paper_dict.items():
                for p in v:
                    if p in paperID_author_dict.keys():
                        if k in L1_field_year_author_dict.keys():
                            L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                        else:
                            L1_field_year_author_dict[k] = []
                            L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        years = list(sorted(L2_field_year_author_dict.keys()))
        print(years)
        for index in range(len(years)):
            current_author = set(L2_field_year_author_dict[years[index]])
            L2_new_author = current_author.difference(cumulation_author)
            for au in current_author:
                cumulation_author.add(au)
            L2_year_new_author_dict[years[index]] = L2_new_author

        begin_year = min(years)
        for k, v in L2_year_new_author_dict.items():
            total_num = 0
            new_num = 0
            max_num = 0
            time_interval = k - begin_year
            if time_interval < 5:
                for v_ in v:
                    max_num_temp = 0
                    for y in range(time_interval + 1):
                        if k-y in L1_field_year_author_dict.keys():
                            temp_num = L1_field_year_author_dict[k-y].count(v_)
                            new_num += temp_num
                            max_num_temp += temp_num
                    if max_num < max_num_temp:
                        max_num = max_num_temp
                for y in range(time_interval + 1):
                    if k - y in L1_field_year_author_dict.keys():
                        total_num += len(L1_field_year_author_dict[k-y])
                if not total_num == 0:
                    L2_year_in_IF_dict[k] = new_num / total_num
                    L2_year_in_IF_max_dict[k] = max_num / total_num
                else:
                    L2_year_in_IF_dict[k] = 0
                    L2_year_in_IF_max_dict[k] = 0
            else:
                for v_ in v:
                    max_num_temp = 0
                    for y in range(5):
                        if k-y in L1_field_year_author_dict.keys():
                            temp_num = L1_field_year_author_dict[k - y].count(v_)
                            new_num += temp_num
                            max_num_temp += temp_num
                    if max_num < max_num_temp:
                        max_num = max_num_temp
                for y in range(5):
                    if k - y in L1_field_year_author_dict.keys():
                        total_num += len(L1_field_year_author_dict[k-y])
                if not total_num == 0:
                    L2_year_in_IF_dict[k] = new_num / total_num
                    L2_year_in_IF_max_dict[k] = max_num / total_num
                else:
                    L2_year_in_IF_dict[k] = 0
                    L2_year_in_IF_max_dict[k] = 0

        save_location = 'E:/PycharmProject/MAG/result/L1twoL2/InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '.txt'
        with open(save_location, 'w') as f:
            f.write(str(L2_year_in_IF_dict))
            f.write('\n')

        save_location = 'E:/PycharmProject/MAG/result/L1twoL2/InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_max.txt'
        with open(save_location, 'w') as f:
            f.write(str(L2_year_in_IF_max_dict))
            f.write('\n')


if __name__ == "__main__":
    # time_IF_diffusion()
    L1 = '0229BD39'
    # time_IF_diffusion_L1twoL2(L1)
    print(find_father_field(L1))