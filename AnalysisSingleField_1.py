import AnalysisSingleField
from collections import Counter
import math


def InnovationDiffusion(L1_field_id, paper_keywords_loc, papers_loc, PaperPerYear_loc, L2FieldPerYear_loc, innovation_loc):
    L2_study_paper_dict = dict()
    L2_study_year_dict = dict()
    paper_year_dict = dict()
    year_num_dict = dict()
    year_L2Field_num_dict = dict()
    child_field_set = AnalysisSingleField.find_child_field(L1_field_id)
    for field in child_field_set:
        L2_study_paper_dict[field] = []
        L2_study_year_dict[field] = []

    with open(paper_keywords_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            field = strline[2].split('\n')[0]
            if field in L2_study_paper_dict.keys():
                L2_study_paper_dict[field].append(strline[0])

    with open(papers_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            paper_year_dict[strline[0]] = int(strline[3])

    for k, v in L2_study_paper_dict.items():
        for f in v:
            L2_study_year_dict[k].append(paper_year_dict[f])

    with open(PaperPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_num_dict[int(strline[0])] = int(num)

    with open(L2FieldPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_L2Field_num_dict[int(strline[0])] = int(num)

    with open(innovation_loc, 'w', encoding='utf-8') as f:
        for k1, v1 in L2_study_year_dict.items():
            temp_dict = dict(Counter(v1))
            result_dict = dict()
            years = list(sorted(temp_dict.keys()))

            for y in years:
                if y < 2015:
                    result_dict[y] = (temp_dict[y] / year_num_dict[y]) * year_L2Field_num_dict[y]

            f.write(k1 + '\t' + str(result_dict) + '\n')
            # for y in years:
            #     if y < 2015:
            #         result_dict[y] = 0
            #         if y in years and y < 2015:
            #             result_dict[y] += (temp_dict[y] / year_num_dict[y]) * year_L2Field_num_dict[y]
            #         result_dict[y] = result_dict[y]
            #
            # f.write(k1 + '\t' + str(result_dict) + '\n')


def InnovationDiffusionSmooth(L1_field_id):
    L2_study_paper_dict = dict()
    L2_study_year_dict = dict()
    paper_year_dict = dict()
    year_num_dict = dict()
    year_L2Field_num_dict = dict()
    child_field_set = AnalysisSingleField.find_child_field(L1_field_id)
    for field in child_field_set:
        L2_study_paper_dict[field] = []
        L2_study_year_dict[field] = []

    with open('E:\PycharmProject\MAG\\result\\034EC4EB_data\paper_keywords.txt', 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            field = strline[2].split('\n')[0]
            if field in L2_study_paper_dict.keys():
                L2_study_paper_dict[field].append(strline[0])

    with open('E:\PycharmProject\MAG\\result\\034EC4EB_data\papers.txt', 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            paper_year_dict[strline[0]] = int(strline[3])

    for k, v in L2_study_paper_dict.items():
        for f in v:
            L2_study_year_dict[k].append(paper_year_dict[f])

    with open('E:\PycharmProject\MAG\\result\PaperPerYear.txt', 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_num_dict[int(strline[0])] = int(num)

    with open('E:\PycharmProject\MAG\\result\L2FieldPerYear.txt', 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_L2Field_num_dict[int(strline[0])] = int(num)

    with open('E:\PycharmProject\MAG\\result\InnovationDiffusion_smooth_2016_034EC4EB_data.txt', 'w', encoding='utf-8') as f:
        for k1, v1 in L2_study_year_dict.items():
            temp_dict = dict(Counter(v1))
            result_dict = dict()
            years = list(sorted(temp_dict.keys()))

            for y in years:
                if y < 2015:
                    temp_sum = 0
                    result_dict[y] = 0
                    num_count = 0
                    for interval in range(-3, 4):
                        if (y + interval in years) and (y + interval) < 2015:
                            result_dict[y] += (temp_dict[y + interval] / year_num_dict[y + interval])
                            num_count += 1
                    result_dict[y] = result_dict[y]/num_count

            f.write(k1 + '\t' + str(result_dict) + '\n')


def InnovationDiffusionSmoothnormalized(L1_field_id, paper_keywords_loc, papers_loc, PaperPerYear_loc, L2FieldPerYear_loc, innovation_loc):
    L2_study_paper_dict = dict()
    L2_study_year_dict = dict()
    paper_year_dict = dict()
    year_num_dict = dict()
    year_L2Field_num_dict = dict()
    child_field_set = AnalysisSingleField.find_child_field(L1_field_id)
    for field in child_field_set:
        L2_study_paper_dict[field] = []
        L2_study_year_dict[field] = []

    with open(paper_keywords_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            field = strline[2].split('\n')[0]
            if field in L2_study_paper_dict.keys():
                L2_study_paper_dict[field].append(strline[0])

    with open(papers_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            paper_year_dict[strline[0]] = int(strline[3])

    for k, v in L2_study_paper_dict.items():
        for f in v:
            L2_study_year_dict[k].append(paper_year_dict[f])

    with open(PaperPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_num_dict[int(strline[0])] = int(num)

    with open(L2FieldPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_L2Field_num_dict[int(strline[0])] = int(num)

    with open(innovation_loc, 'w', encoding='utf-8') as f:
        for k1, v1 in L2_study_year_dict.items():
            temp_dict = dict(Counter(v1))
            result_dict = dict()
            years = list(sorted(temp_dict.keys()))
            # if k1 == '03DD8204':
            #     print(temp_dict)
            for y in years:
                if y < 2015:
                    result_dict[y] = 0
                    num_count = 0
                    for interval in range(-3, 4):
                        if (y + interval in years) and (y + interval) < 2015:
                            result_dict[y] += (temp_dict[y + interval] / year_num_dict[y + interval]) * year_L2Field_num_dict[y + interval]
                            num_count += 1
                    result_dict[y] = result_dict[y]/num_count
            # if k1 == '03DD8204':
            #     print(result_dict)
            f.write(k1 + '\t' + str(result_dict) + '\n')


def time_IF_diffusion_L1twoL2(L1_field_ID):
    child_field_set = AnalysisSingleField.find_child_field(L1_field_ID)
    print(child_field_set)

    for c_field in child_field_set:
        L2_field_paper = set()
        L1_field_paper = set()
        L2_field_year_paper_dict = dict()
        L1_field_year_paper_dict = dict()
        L2_field_year_author_dict = dict()
        L1_field_year_author_dict = dict()
        paperID_author_dict = dict()
        cumulation_author = set()
        current_author = set()
        L2_year_new_author_dict = dict()
        L2_year_in_IF_dict = dict()
        L2_year_in_IF_max_dict = dict()
        max_IF_people_year_dict = dict()
        five_year_later_people_at_dict = dict()
        father_field_set = AnalysisSingleField.find_father_field(c_field)

        with open('E:\PycharmProject\MAG\\result\\0229BD39_data\paper_keywords.txt', 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                field = strline[2].split('\n')[0]
                if field == c_field:
                    L2_field_paper.add(strline[0])
                elif field in father_field_set:
                    L1_field_paper.add(strline[0])

        with open('E:\PycharmProject\MAG\\result\\0229BD39_data\papers.txt', 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if strline[0] in L2_field_paper:
                    year = int(strline[3])
                    if year in L2_field_year_paper_dict.keys():
                        L2_field_year_paper_dict[year].append(strline[0])
                    else:
                        L2_field_year_paper_dict[year] = []
                        L2_field_year_paper_dict[year].append(strline[0])
                elif strline[0] in L1_field_paper:
                    year = int(strline[3])
                    if year in L1_field_year_paper_dict.keys():
                        L1_field_year_paper_dict[year].append(strline[0])
                    else:
                        L1_field_year_paper_dict[year] = []
                        L1_field_year_paper_dict[year].append(strline[0])

        with open('E:\PycharmProject\MAG\\result\\0229BD39_data\paper_author_affiliations.txt', 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if len(strline) > 5:
                    if len(strline) > 3 and len(strline[1]) == 8:
                        if strline[0] in paperID_author_dict.keys():
                            paperID_author_dict[strline[0]].append(strline[1])
                        else:
                            paperID_author_dict[strline[0]] = []
                            paperID_author_dict[strline[0]].append(strline[1])

        for k, v in L2_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L2_field_year_author_dict.keys():
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L2_field_year_author_dict[k] = []
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        for k, v in L1_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L1_field_year_author_dict.keys():
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L1_field_year_author_dict[k] = []
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        years = list(sorted(L2_field_year_author_dict.keys()))
        print(years)
        for index in range(len(years)):
            current_author = set(L2_field_year_author_dict[years[index]])
            L2_new_author = current_author.difference(cumulation_author)
            for au in current_author:
                cumulation_author.add(au)
            L2_year_new_author_dict[years[index]] = L2_new_author

        begin_year = min(years)
        for k, v in L2_year_new_author_dict.items():
            total_num = 0
            new_num = 0
            max_num = 0
            time_interval = k - begin_year
            if time_interval < 5:
                for v_ in v:
                    max_num_temp = 0
                    # max_author = ''
                    for y in range(time_interval + 1):
                        if k-y in L1_field_year_author_dict.keys():
                            temp_num = L1_field_year_author_dict[k-y].count(v_)

                            new_num += temp_num
                            max_num_temp += temp_num
                    if max_num < max_num_temp:
                        max_num = max_num_temp
                        max_IF_people_year_dict[v_] = k
                        # max_author = v_
                for y in range(time_interval + 1):
                    if k - y in L1_field_year_author_dict.keys():
                        total_num += len(L1_field_year_author_dict[k-y])

                if not total_num == 0:
                    L2_year_in_IF_dict[k] = new_num / total_num
                    L2_year_in_IF_max_dict[k] = max_num / total_num
                else:
                    L2_year_in_IF_dict[k] = 0
                    L2_year_in_IF_max_dict[k] = 0
            else:
                for v_ in v:
                    max_num_temp = 0
                    for y in range(5):
                        if k-y in L1_field_year_author_dict.keys():
                            temp_num = L1_field_year_author_dict[k - y].count(v_)
                            new_num += temp_num
                            max_num_temp += temp_num
                    if max_num < max_num_temp:
                        max_num = max_num_temp
                        max_IF_people_year_dict[v_] = k
                for y in range(5):
                    if k - y in L1_field_year_author_dict.keys():
                        total_num += len(L1_field_year_author_dict[k-y])
                if not total_num == 0:
                    L2_year_in_IF_dict[k] = new_num / total_num
                    L2_year_in_IF_max_dict[k] = max_num / total_num
                else:
                    L2_year_in_IF_dict[k] = 0
                    L2_year_in_IF_max_dict[k] = 0

        # for k1, v1 in max_IF_people_year_dict.items():
        #     five_year_later_people_at_dict[k1] = 0

        for k1, v1 in max_IF_people_year_dict.items():
            for k2, v2 in L2_field_year_author_dict.items():
                if k1 in v2:
                    if k2 > (v1 + 4):
                        # five_year_later_people_at_dict[k1] = 1
                        five_year_later_people_at_dict[v1] = 1

        save_location = 'E:\PycharmProject\MAG\\result\\0229BD39_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_5YearLater.txt'
        with open(save_location, 'w') as f:
            f.write(str(five_year_later_people_at_dict))
            f.write('\n')

        save_location = 'E:\PycharmProject\MAG\\result\\0229BD39_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '.txt'
        with open(save_location, 'w') as f:
            f.write(str(L2_year_in_IF_dict))
            f.write('\n')

        save_location = 'E:\PycharmProject\MAG\\result\\0229BD39_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_max.txt'
        with open(save_location, 'w') as f:
            f.write(str(L2_year_in_IF_max_dict))
            f.write('\n')


def time_IF_diffusion_L1twoL2_normalized(L1_field_ID):
    child_field_set = AnalysisSingleField.find_child_field(L1_field_ID)
    print(child_field_set)

    for c_field in child_field_set:
        L2_field_paper = set()
        L1_field_paper = set()
        L2_field_year_paper_dict = dict()
        L1_field_year_paper_dict = dict()
        L2_field_year_author_dict = dict()
        L1_field_year_author_dict = dict()
        paperID_author_dict = dict()
        cumulation_author = set()
        current_author = set()
        L2_year_new_author_dict = dict()
        L2_year_in_IF_dict = dict()
        L2_year_in_IF_max_dict = dict()
        max_IF_people_year_dict = dict()
        five_year_later_people_at_dict = dict()

        father_field_set = AnalysisSingleField.find_father_field(c_field)

        with open('E:\PycharmProject\MAG\\result\\0229BD39_data\paper_keywords.txt', 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                field = strline[2].split('\n')[0]
                if field == c_field:
                    L2_field_paper.add(strline[0])
                elif field in father_field_set:
                    L1_field_paper.add(strline[0])

        with open('E:\PycharmProject\MAG\\result\\0229BD39_data\papers.txt', 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if strline[0] in L2_field_paper:
                    year = int(strline[3])
                    if year in L2_field_year_paper_dict.keys():
                        L2_field_year_paper_dict[year].append(strline[0])
                    else:
                        L2_field_year_paper_dict[year] = []
                        L2_field_year_paper_dict[year].append(strline[0])
                elif strline[0] in L1_field_paper:
                    year = int(strline[3])
                    if year in L1_field_year_paper_dict.keys():
                        L1_field_year_paper_dict[year].append(strline[0])
                    else:
                        L1_field_year_paper_dict[year] = []
                        L1_field_year_paper_dict[year].append(strline[0])

        with open('E:\PycharmProject\MAG\\result\\0229BD39_data\paper_author_affiliations.txt', 'r',
                  encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if len(strline) > 5:
                    if len(strline) > 3 and len(strline[1]) == 8:
                        if strline[0] in paperID_author_dict.keys():
                            paperID_author_dict[strline[0]].append(strline[1])
                        else:
                            paperID_author_dict[strline[0]] = []
                            paperID_author_dict[strline[0]].append(strline[1])

        for k, v in L2_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L2_field_year_author_dict.keys():
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L2_field_year_author_dict[k] = []
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        for k, v in L1_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L1_field_year_author_dict.keys():
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L1_field_year_author_dict[k] = []
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        years = list(sorted(L2_field_year_author_dict.keys()))
        print(years)
        for index in range(len(years)):
            current_author = set(L2_field_year_author_dict[years[index]])
            L2_new_author = current_author.difference(cumulation_author)
            for au in current_author:
                cumulation_author.add(au)
            L2_year_new_author_dict[years[index]] = L2_new_author

        begin_year = min(years)
        for k, v in L2_year_new_author_dict.items():
            total_num = 0
            new_num = 0
            max_num = 0
            author_num = 0
            max_author = ''
            time_interval = k - begin_year
            if time_interval < 4:
                for v_ in v:
                    max_num_temp = 0
                    # max_author = ''
                    for y in range(time_interval + 1):
                        if k - y in L1_field_year_author_dict.keys():
                            temp_num = L1_field_year_author_dict[k - y].count(v_)
                            if not len(L1_field_year_author_dict[k - y]) == 0:
                                # new_num += (temp_num / len(L1_field_year_author_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                                # max_num_temp += (temp_num / len(L1_field_year_author_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                                new_num += (temp_num / len(L1_field_year_paper_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                                max_num_temp += (temp_num / len(L1_field_year_paper_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                            else:
                                new_num += 0
                                max_num_temp += 0
                    if max_num < max_num_temp:
                        max_num = max_num_temp
                        # max_IF_people_year_dict[v_] = k
                        max_author = v_
                max_IF_people_year_dict[max_author] = k
                num_count = 0
                for y in range(time_interval + 1):
                    if k - y in L1_field_year_author_dict.keys():
                        num_count += 1
                if not num_count == 0:
                    L2_year_in_IF_dict[k] = new_num / num_count
                    L2_year_in_IF_max_dict[k] = max_num / num_count
                else:
                    L2_year_in_IF_dict[k] = 0
                    L2_year_in_IF_max_dict[k] = 0
            else:
                for v_ in v:
                    max_num_temp = 0
                    for y in range(5):
                        if k - y in L1_field_year_author_dict.keys():
                            temp_num = L1_field_year_author_dict[k - y].count(v_)
                            if not len(L1_field_year_author_dict[k - y]) == 0:
                                # new_num += (temp_num / len(L1_field_year_author_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                                # max_num_temp += (temp_num / len(L1_field_year_author_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                                new_num += (temp_num / len(L1_field_year_paper_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                                max_num_temp += (temp_num / len(L1_field_year_paper_dict[k - y])) * len(set(L1_field_year_author_dict[k - y]))
                            else:
                                new_num += 0
                                max_num_temp += 0
                    if max_num < max_num_temp:
                        max_num = max_num_temp
                        # max_IF_people_year_dict[v_] = k
                        max_author = v_
                max_IF_people_year_dict[max_author] = k
                num_count = 0
                for y in range(5):
                    if k - y in L1_field_year_author_dict.keys():
                        num_count += 1
                if not num_count == 0:
                    L2_year_in_IF_dict[k] = new_num / num_count
                    L2_year_in_IF_max_dict[k] = max_num / num_count
                else:
                    L2_year_in_IF_dict[k] = 0
                    L2_year_in_IF_max_dict[k] = 0

        # for k1, v1 in max_IF_people_year_dict.items():
        #     five_year_later_people_at_dict[k1] = 0

        for k1, v1 in max_IF_people_year_dict.items():
            for k2, v2 in L2_field_year_author_dict.items():
                if k1 in v2:
                    if k2 > (v1 + 4):
                        # five_year_later_people_at_dict[k1] = 1
                        five_year_later_people_at_dict[v1] = 1

        save_location = 'E:\PycharmProject\MAG\\result\\0229BD39_data_L1twoL2_5year_later\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_5YearLater.txt'
        with open(save_location, 'w') as f:
            f.write(str(five_year_later_people_at_dict))
            f.write('\n')

        save_location = 'E:\PycharmProject\MAG\\result\\0229BD39_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '.txt'
        with open(save_location, 'w') as f:
            f.write(str(L2_year_in_IF_dict))
            f.write('\n')

        save_location = 'E:\PycharmProject\MAG\\result\\0229BD39_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_max.txt'
        with open(save_location, 'w') as f:
            f.write(str(L2_year_in_IF_max_dict))
            f.write('\n')


def time_IF_diffusion_L1twoL2_paper_rank(L1_field_ID, paper_keywords_loc, papers_loc, paper_author_affiliations_loc,
                                         L2FieldPerYear_loc, author_per_year_loc):
    child_field_set = AnalysisSingleField.find_child_field(L1_field_ID)
    print(child_field_set)
    year_L2Field_num_dict = dict()
    year_author_num_dict = dict()

    with open(L2FieldPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_L2Field_num_dict[int(strline[0])] = int(num)

    with open(author_per_year_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_author_num_dict[int(strline[0])] = int(num)

    for c_field in child_field_set:
        L2_field_paper = set()
        L1_field_paper = set()
        L2_field_year_paper_dict = dict()
        L1_field_year_paper_dict = dict()
        L2_field_year_author_dict = dict()
        L1_field_year_author_dict = dict()
        paperID_author_dict = dict()
        paperID_rank_dict = dict()
        cumulation_author = set()
        current_author = set()
        L2_year_new_author_dict = dict()
        L2_year_in_IF_dict = dict()
        L2_year_in_IFsmooth_dict = dict()
        L2_year_in_IF_max_dict = dict()
        max_IF_people_year_dict = dict()
        five_year_later_people_at_dict = dict()
        L2_year_new_author_author_dict = dict()
        L2_year_new_author_number_dict = dict()
        L2_year_new_author_author_smooth_dict = dict()
        L2_year_average_IF = dict()

        father_field_set = AnalysisSingleField.find_father_field(c_field)

        with open(paper_keywords_loc, 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                field = strline[2].split('\n')[0]
                if field == c_field:
                    L2_field_paper.add(strline[0])
                elif field in father_field_set:
                    L1_field_paper.add(strline[0])

        with open(papers_loc, 'r', encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                rank = int(strline[10])
                paperID_rank_dict[strline[0]] = math.exp(rank / (-1000))
                if strline[0] in L2_field_paper:
                    year = int(strline[3])
                    if year in L2_field_year_paper_dict.keys():
                        L2_field_year_paper_dict[year].append(strline[0])
                    else:
                        L2_field_year_paper_dict[year] = []
                        L2_field_year_paper_dict[year].append(strline[0])
                elif strline[0] in L1_field_paper:
                    year = int(strline[3])
                    if year in L1_field_year_paper_dict.keys():
                        L1_field_year_paper_dict[year].append(strline[0])
                    else:
                        L1_field_year_paper_dict[year] = []
                        L1_field_year_paper_dict[year].append(strline[0])

        with open(paper_author_affiliations_loc, 'r',
                  encoding='utf-8') as f:
            for line in f:
                strline = line.split('\t')
                if len(strline) > 5:
                    if len(strline) > 3 and len(strline[1]) == 8:
                        if strline[0] in paperID_author_dict.keys():
                            paperID_author_dict[strline[0]].append(strline[1])
                        else:
                            paperID_author_dict[strline[0]] = []
                            paperID_author_dict[strline[0]].append(strline[1])

        for k, v in L2_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L2_field_year_author_dict.keys():
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L2_field_year_author_dict[k] = []
                        L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        for k, v in L1_field_year_paper_dict.items():
            for p in v:
                if p in paperID_author_dict.keys():
                    if k in L1_field_year_author_dict.keys():
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                    else:
                        L1_field_year_author_dict[k] = []
                        L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

        # for k, v in L2_field_year_paper_dict.items():
        #     L2_year_average_IF[k] = 0
        #     for p in v:
        #         L2_year_average_IF[k] += paperID_rank_dict[p]
        #     L2_year_average_IF[k] = L2_year_average_IF[k]/len(v)

        years = list(sorted(L2_field_year_paper_dict.keys()))
        print(years)
        for index in range(len(years)):
            current_author = set(L2_field_year_author_dict[years[index]])
            L2_new_author = current_author.difference(cumulation_author)
            for au in current_author:
                cumulation_author.add(au)
            L2_year_new_author_dict[years[index]] = L2_new_author

        if not len(years) == 0:
            begin_year = min(years)
            for k, v in L2_year_new_author_dict.items():
                total_num = 0
                new_IF = 0
                new_num = 0
                max_IF = 0
                max_num = 0
                author_num = 0
                max_author = ''
                time_interval = k - begin_year
                author_IF_dict = dict()
                if time_interval < 4:
                    for y in range(time_interval + 1):
                        if k - y in L1_field_year_paper_dict.keys():
                            for paper in L1_field_year_paper_dict[k - y]:
                                authors = paperID_author_dict[paper]
                                for a in authors:
                                    if a in author_IF_dict.keys():
                                        author_IF_dict[a] += paperID_rank_dict[paper]
                                    else:
                                        author_IF_dict[a] = paperID_rank_dict[paper]
                else:
                    for y in range(5):
                        if k - y in L1_field_year_paper_dict.keys():
                            for paper in L1_field_year_paper_dict[k - y]:
                                authors = paperID_author_dict[paper]
                                for a in authors:
                                    if a in author_IF_dict.keys():
                                        author_IF_dict[a] += paperID_rank_dict[paper]
                                    else:
                                        author_IF_dict[a] = paperID_rank_dict[paper]

                for v_ in v:
                    if v_ in author_IF_dict.keys():
                        temp_IF = author_IF_dict[v_]
                        new_IF += temp_IF
                        if max_IF < temp_IF:
                            max_IF = temp_IF
                            max_author = v_

                max_IF_people_year_dict[max_author] = k
                L2_year_in_IF_dict[k] = new_IF
                L2_year_in_IF_max_dict[k] = max_IF

            for k1, v1 in max_IF_people_year_dict.items():
                for k2, v2 in L2_field_year_author_dict.items():
                    if k1 in v2:
                        if k2 > (v1 + 4):
                            five_year_later_people_at_dict[v1] = 1

            for y in years:
                if y < 2015:
                    L2_year_in_IFsmooth_dict[y] = 0
                    num_count = 0
                    for interval in range(-3, 4):
                        if (y + interval in years) and (y + interval) < 2015:
                            L2_year_in_IFsmooth_dict[y] += L2_year_in_IF_dict[y + interval]
                            num_count += 1
                    L2_year_in_IFsmooth_dict[y] = L2_year_in_IFsmooth_dict[y]/num_count

            for k, v in L2_year_new_author_dict.items():
                authors_num = 0
                norm_authors_num = 0
                year_author_num = 0
                year_field_num = 0
                for interval in range(-4, 1):
                    if (k + interval in years) and (k + interval) < 2015:
                        authors_num += len(set(L2_field_year_author_dict[k + interval]))
                        # year_author_num += year_author_num_dict[k + interval]
                        # year_field_num += year_L2Field_num_dict[k + interval]
                        norm_authors_num += (len(set(L2_field_year_author_dict[k + interval]))/year_author_num_dict[k + interval]) * year_L2Field_num_dict[k + interval]
                        # total_author_num += year_author_num_dict[k + interval]
                L2_year_new_author_author_dict[k] = len(set(v))/authors_num
                L2_year_new_author_number_dict[k] = str((len(set(v))/year_author_num_dict[k])*year_L2Field_num_dict[k]) \
                                                    + '_' + str(norm_authors_num/5)

            for y in years:
                count = 0
                num = 0
                for interval in range(-3, 4):
                    if (y + interval in years) and (y + interval) < 2015:
                        count += L2_year_new_author_author_dict[y + interval]
                        num += 1
                L2_year_new_author_author_smooth_dict[y] = count/num

            save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2_5year_later\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_5YearLater.txt'
            with open(save_location, 'w') as f:
                f.write(str(five_year_later_people_at_dict))
                f.write('\n')

            save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '.txt'
            with open(save_location, 'w') as f:
                f.write(str(L2_year_in_IF_dict))
                f.write('\n')

            save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_max.txt'
            with open(save_location, 'w') as f:
                f.write(str(L2_year_in_IF_max_dict))
                f.write('\n')

            save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_smooth.txt'
            with open(save_location, 'w') as f:
                f.write(str(L2_year_in_IFsmooth_dict))
                f.write('\n')

            save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_new_author.txt'
            with open(save_location, 'w') as f:
                f.write(str(L2_year_new_author_author_dict))
                f.write('\n')

            save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_new_author_number.txt'
            with open(save_location, 'w') as f:
                f.write(str(L2_year_new_author_number_dict))
                f.write('\n')

            save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2_author\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_author_smooth.txt'
            with open(save_location, 'w') as f:
                f.write(str(L2_year_new_author_author_smooth_dict))
                f.write('\n')

            # save_location = 'E:\PycharmProject\MAG\\result\\' + L1_field_ID + '_data_L1twoL2\InnovationDiffusion_in_IF_2016_' + L1_field_ID + '_' + c_field + '_average_IF.txt'
            # with open(save_location, 'w') as f:
            #     f.write(str(L2_year_average_IF))
            #     f.write('\n')


def new_type_author(L1_field_ID, paper_keywords_loc, papers_loc, paper_author_affiliations_loc,
                                         L2FieldPerYear_loc, author_per_year_loc):
    child_field_set = AnalysisSingleField.find_child_field(L1_field_ID)
    print(child_field_set)
    year_L2Field_num_dict = dict()
    year_author_num_dict = dict()

    with open(L2FieldPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_L2Field_num_dict[int(strline[0])] = int(num)

    with open(author_per_year_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_author_num_dict[int(strline[0])] = int(num)

    with open('E:\PycharmProject\MAG\\result\InnovationDiffusionNewAuthor_' + L1_id + '_2016.txt', 'w', encoding='utf-8') as f_r:
        for c_field in child_field_set:
            L2_field_paper = set()
            L1_field_paper = set()
            L2_field_year_paper_dict = dict()
            L1_field_year_paper_dict = dict()
            L2_field_year_author_dict = dict()
            L1_field_year_author_dict = dict()
            paperID_author_dict = dict()
            paperID_rank_dict = dict()
            cumulation_author = set()
            L2_year_new_author_dict = dict()
            new_authors_paper_num_dict = dict()
    
            father_field_set = AnalysisSingleField.find_father_field(c_field)
    
            with open(paper_keywords_loc, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    field = strline[2].split('\n')[0]
                    if field == c_field:
                        L2_field_paper.add(strline[0])
                    elif field in father_field_set:
                        L1_field_paper.add(strline[0])
    
            with open(papers_loc, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    rank = int(strline[10])
                    paperID_rank_dict[strline[0]] = math.exp(rank / (-1000))
                    if strline[0] in L2_field_paper:
                        year = int(strline[3])
                        if year in L2_field_year_paper_dict.keys():
                            L2_field_year_paper_dict[year].append(strline[0])
                        else:
                            L2_field_year_paper_dict[year] = []
                            L2_field_year_paper_dict[year].append(strline[0])
                    elif strline[0] in L1_field_paper:
                        year = int(strline[3])
                        if year in L1_field_year_paper_dict.keys():
                            L1_field_year_paper_dict[year].append(strline[0])
                        else:
                            L1_field_year_paper_dict[year] = []
                            L1_field_year_paper_dict[year].append(strline[0])
    
            with open(paper_author_affiliations_loc, 'r',
                      encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    if len(strline) > 5:
                        if len(strline) > 3 and len(strline[1]) == 8:
                            if strline[0] in paperID_author_dict.keys():
                                paperID_author_dict[strline[0]].append(strline[1])
                            else:
                                paperID_author_dict[strline[0]] = []
                                paperID_author_dict[strline[0]].append(strline[1])
    
            for k, v in L2_field_year_paper_dict.items():
                for p in v:
                    if p in paperID_author_dict.keys():
                        if k in L2_field_year_author_dict.keys():
                            L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                        else:
                            L2_field_year_author_dict[k] = []
                            L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
    
            for k, v in L1_field_year_paper_dict.items():
                for p in v:
                    if p in paperID_author_dict.keys():
                        if k in L1_field_year_author_dict.keys():
                            L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                        else:
                            L1_field_year_author_dict[k] = []
                            L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
    
            years = list(sorted(L2_field_year_paper_dict.keys()))
            print(years)
            for index in range(len(years)):
                current_author = set(L2_field_year_author_dict[years[index]])
                L2_new_author = current_author.difference(cumulation_author)
                for au in current_author:
                    cumulation_author.add(au)
                L2_year_new_author_dict[years[index]] = L2_new_author
    
            for k, v in L2_field_year_paper_dict.items():
                new_authors = L2_year_new_author_dict[k]
                new_authors_paper_num = 0
                for p in v:
                    if p in paperID_author_dict.keys():
                        authors = paperID_author_dict[p]
                        for a in authors:
                            if a in new_authors:
                                new_authors_paper_num += 1
                                break
                new_authors_paper_num_dict[k] = (new_authors_paper_num / year_author_num_dict[k])*year_L2Field_num_dict[k]
            f_r.write(c_field + '\t' + str(new_authors_paper_num_dict) + '\n')


def different_type_author(L1_field_ID, paper_keywords_loc, papers_loc, paper_author_affiliations_loc,
                    L2FieldPerYear_loc, paper_per_year_loc, type_function):

    child_field_set = AnalysisSingleField.find_child_field(L1_field_ID)
    print(child_field_set)
    year_L2Field_num_dict = dict()
    year_paper_num_dict = dict()

    with open(L2FieldPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_L2Field_num_dict[int(strline[0])] = int(num)

    with open(paper_per_year_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_paper_num_dict[int(strline[0])] = int(num)

    with open('E:\PycharmProject\MAG\\result\InnovationDiffusion_' + L1_id + '_' + str(type_function) + '_2016.txt', 'w',
              encoding='utf-8') as f_r:
        for c_field in child_field_set:
            L2_field_paper = set()
            L1_field_paper = set()
            L2_field_year_paper_dict = dict()
            L1_field_year_paper_dict = dict()
            L2_field_year_author_dict = dict()
            L1_field_year_author_dict = dict()
            paperID_author_dict = dict()
            paperID_rank_dict = dict()
            cumulation_author = set()
            L2_year_new_author_dict = dict()
            authors_result_dict = dict()

            father_field_set = AnalysisSingleField.find_father_field(c_field)

            with open(paper_keywords_loc, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    field = strline[2].split('\n')[0]
                    if field == c_field:
                        L2_field_paper.add(strline[0])
                    elif field in father_field_set:
                        L1_field_paper.add(strline[0])

            with open(papers_loc, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    rank = int(strline[10])
                    paperID_rank_dict[strline[0]] = math.exp(rank / (-1000))
                    if strline[0] in L2_field_paper:
                        year = int(strline[3])
                        if year in L2_field_year_paper_dict.keys():
                            L2_field_year_paper_dict[year].append(strline[0])
                        else:
                            L2_field_year_paper_dict[year] = []
                            L2_field_year_paper_dict[year].append(strline[0])
                    elif strline[0] in L1_field_paper:
                        year = int(strline[3])
                        if year in L1_field_year_paper_dict.keys():
                            L1_field_year_paper_dict[year].append(strline[0])
                        else:
                            L1_field_year_paper_dict[year] = []
                            L1_field_year_paper_dict[year].append(strline[0])

            with open(paper_author_affiliations_loc, 'r',
                      encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    if len(strline) > 5:
                        if len(strline) > 3 and len(strline[1]) == 8:
                            if strline[0] in paperID_author_dict.keys():
                                paperID_author_dict[strline[0]].append(strline[1])
                            else:
                                paperID_author_dict[strline[0]] = []
                                paperID_author_dict[strline[0]].append(strline[1])

            for k, v in L2_field_year_paper_dict.items():
                for p in v:
                    if p in paperID_author_dict.keys():
                        if k in L2_field_year_author_dict.keys():
                            L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                        else:
                            L2_field_year_author_dict[k] = []
                            L2_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

            for k, v in L1_field_year_paper_dict.items():
                for p in v:
                    if p in paperID_author_dict.keys():
                        if k in L1_field_year_author_dict.keys():
                            L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())
                        else:
                            L1_field_year_author_dict[k] = []
                            L1_field_year_author_dict[k].extend(paperID_author_dict[p].copy())

            years = list(sorted(L2_field_year_paper_dict.keys()))
            print(years)
            for index in range(len(years)):
                current_author = set(L2_field_year_author_dict[years[index]])
                L2_new_author = current_author.difference(cumulation_author)
                for au in current_author:
                    cumulation_author.add(au)
                L2_year_new_author_dict[years[index]] = L2_new_author

            for k, v in L2_field_year_paper_dict.items():
                # print(k, len(v))
                new_authors = L2_year_new_author_dict[k]
                new_authors_paper_num = 0
                all_new_authors_paper_num = 0

                for p in v:
                    flag = True
                    # if p in paperID_author_dict.keys():
                    authors = paperID_author_dict[p]
                    for a in authors:
                        if a in new_authors:
                            new_authors_paper_num += 1
                            break

                    temp_count = 0
                    for a in authors:
                        if a in new_authors:
                            temp_count += 1

                    if temp_count == len(authors):
                        all_new_authors_paper_num += 1

                if type_function == 1:
                    authors_result_dict[k] = (new_authors_paper_num / year_paper_num_dict[k]) * \
                                             year_L2Field_num_dict[k]
                    # # authors_result_dict[k] = (len(v) / year_paper_num_dict[k]) * \
                    # #                          year_L2Field_num_dict[k]
                    # authors_result_dict[k] = len(v)
                elif type_function == 2:
                    old_authors_paper_num = len(v) - new_authors_paper_num
                    authors_result_dict[k] = (old_authors_paper_num / year_paper_num_dict[k]) * \
                                                    year_L2Field_num_dict[k]
                elif type_function == 3:
                    authors_result_dict[k] = new_authors_paper_num / len(v)
                elif type_function == 4:
                    authors_result_dict[k] = (all_new_authors_paper_num / year_paper_num_dict[k]) * \
                                             year_L2Field_num_dict[k]
                elif type_function == 5:
                    authors_result_dict[k] = all_new_authors_paper_num / len(v)
            f_r.write(c_field + '\t' + str(authors_result_dict) + '\n')


def count_field_author_paper_num(paper_author_affiliations_location):
    paper_set = set()
    author_set = set()
    with open(paper_author_affiliations_location, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            paper_set.add(strline[0])
            author_set.add(strline[1])

    print('文章数：', len(paper_set))
    print('作者数：', len(author_set))


def year_average_IF(L1_field_ID, paper_keywords_loc, papers_loc,
                                         L2FieldPerYear_loc, author_per_year_loc):
    child_field_set = AnalysisSingleField.find_child_field(L1_field_ID)
    print(child_field_set)
    year_L2Field_num_dict = dict()
    year_author_num_dict = dict()

    with open(L2FieldPerYear_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_L2Field_num_dict[int(strline[0])] = int(num)

    with open(author_per_year_loc, 'r', encoding='utf-8') as f:
        for line in f:
            strline = line.split('\t')
            num = strline[1].split('\n')[0]
            year_author_num_dict[int(strline[0])] = int(num)

    with open('E:\PycharmProject\MAG\\result\\' + L1_id + '_2016_average_IF.txt', 'w') as f_r:
        for c_field in child_field_set:
            L2_field_paper = set()
            L1_field_paper = set()
            L2_field_year_paper_dict = dict()
            L1_field_year_paper_dict = dict()
            paperID_rank_dict = dict()
            L2_year_average_IF = dict()

            father_field_set = AnalysisSingleField.find_father_field(c_field)

            with open(paper_keywords_loc, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    field = strline[2].split('\n')[0]
                    if field == c_field:
                        L2_field_paper.add(strline[0])
                    elif field in father_field_set:
                        L1_field_paper.add(strline[0])

            with open(papers_loc, 'r', encoding='utf-8') as f:
                for line in f:
                    strline = line.split('\t')
                    rank = int(strline[10])
                    paperID_rank_dict[strline[0]] = math.exp(rank / (-1000))
                    if strline[0] in L2_field_paper:
                        year = int(strline[3])
                        if year in L2_field_year_paper_dict.keys():
                            L2_field_year_paper_dict[year].append(strline[0])
                        else:
                            L2_field_year_paper_dict[year] = []
                            L2_field_year_paper_dict[year].append(strline[0])

            for k, v in L2_field_year_paper_dict.items():
                L2_year_average_IF[k] = 0
                for p in v:
                    L2_year_average_IF[k] += paperID_rank_dict[p]
                L2_year_average_IF[k] = L2_year_average_IF[k]/len(v)

            print(c_field)
            f_r.write(c_field + '\t' + str(L2_year_average_IF))
            f_r.write('\n')


if __name__ == "__main__":
    L1_id = '0229BD39'
    paper_keywords_location = 'E:\PycharmProject\MAG\\result\\' + L1_id + '_data\paper_keywords.txt'
    papers_location = 'E:\PycharmProject\MAG\\result\\' + L1_id + '_data\papers.txt'
    paper_author_affiliations_location = 'E:\PycharmProject\MAG\\result\\' + L1_id + '_data\paper_author_affiliations.txt'
    PaperPerYear_location = 'E:\PycharmProject\MAG\\result\PaperPerYear.txt'
    L2FieldPerYear_location = 'E:\PycharmProject\MAG\\result\L2FieldPerYear.txt'
    innovation_location = 'E:\PycharmProject\MAG\\result\InnovationDiffusion_' + L1_id + '_2016.txt'
    innovation_location_smooth = 'E:\PycharmProject\MAG\\result\InnovationDiffusion_' + L1_id + '_2016_smooth.txt'
    author_per_year_location = 'E:\PycharmProject\MAG\\result\AuthorPerYear.txt'

    # InnovationDiffusion(L1_id, paper_keywords_location, papers_location, PaperPerYear_location, L2FieldPerYear_location,
    #                     innovation_location)
    #
    # InnovationDiffusionSmoothnormalized(L1_id, paper_keywords_location, papers_location, PaperPerYear_location,
    #                                     L2FieldPerYear_location, innovation_location_smooth)
    #
    # # different_type_author(L1_id, paper_keywords_location, papers_location, paper_author_affiliations_location,
    # #                                      L2FieldPerYear_location, PaperPerYear_location, 5)
    #
    time_IF_diffusion_L1twoL2_paper_rank(L1_id, paper_keywords_location, papers_location, paper_author_affiliations_location,
                                         L2FieldPerYear_location, author_per_year_location)
    year_average_IF(L1_id, paper_keywords_location, papers_location,
                                         L2FieldPerYear_location, author_per_year_location)

    count_field_author_paper_num(paper_author_affiliations_location)